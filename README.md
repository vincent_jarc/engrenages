<center>

# ENGRENAGES

</center>

- [Description](#description)
- [Prérequis](#prerequis)
- [Installation](#installation)
- [Utilisation](#utilisation)
- [Licence](#licence)
- [Bugs connus](#bugs-connus)
- [Roadmap](#roadmap)

## Description

***Engrenage*** est une application python permettant de générer plusieurs engrenages en quelques clics pour vos exercices et devoirs d'arithmétique. Indiquez simplement le nombre de dents souhaitées et l'orientation voulue. Vous pouvez déplacer, tourner ou imbriquer vos engrenages comme bon vous semble. 

## Prérequis

- Python 3.6 ou version ultérieure

## Installation

- Dans le menu `Code`, dans la rubrique `Téléchargez le code source` choisissez `zip`. 
- Décompressez l'archive téléchargée
- Exécutez le fichier `main.py` : 
    - sous Windows : un double-clic devrait exécuter le script. Si le fichier s'ouvre avec **Idle** , appuyez sur `F5` ou sur le menu `Run > Run module`. 
    - sous MacOS : faites un clic droit puis `Ouvrir avec > Python Launcher`. 

> **WARNING** :warning:
> Si vous n'avez jamais installé **Python**, la dernière version est téléchargeable à [l'adresse suivante](https://www.python.org/downloads/). 

## Utilisation

- ![Ajouter](https://gitlab.com/vincent_jarc/engrenages/-/raw/main/pics/plus.png) : permet d'ajouter un nouvel engrenage d'un simple clic. Pensez à paramétrer le nombre de dents au préalable et éventuellement l'orientation. 
    - **Nombre de dents** et **taille** : les dents de l'engrenage mesurent toutes 25 pixels de côté. Plus un engrenage aura de dents, plus il sera grand.
    - **Orientation** : indique l'angle formé par la première dent, le centre de l'engrenage et la verticale (vecteur $\binom{0}{1}$). L'angle est mesuré dans le sens trigonométrique mais apparaît en degrés. 
    - **Couleur** : la couleur de l'engrenage est générée automatiquement. Une personnalisation des couleurs sera proposée dans une version ultérieure. 
- ![Supprimer](https://gitlab.com/vincent_jarc/engrenages/-/raw/main/pics/effacer.png) : permet de supprimer un engrenage. Cliquez sur le(s) engrenage(s) à effacer. 
- ![Propriétés](https://gitlab.com/vincent_jarc/engrenages/-/raw/main/pics/reglage.png) : permet de voir les propriétés d'un engrenage. Cliquez sur ce bouton puis faites un clic gauche sur l'engrenage désiré. Un simple clic droit fera également l'affaire. 
- ![Déplacer](https://gitlab.com/vincent_jarc/engrenages/-/raw/main/pics/deplacer.png) : permet de déplacer un engrenage. Cliquez sur un engrenage, celui-ci suivra alors votre souris. Cliquez à nouveau pour le fixer. 
- ![Tourner](https://gitlab.com/vincent_jarc/engrenages/-/raw/main/pics/tourner.png) : permet de modifier l'orientation d'un engrenage. Cliquez sur un engrenage, plus vous déplacerez votre souris, plus il tournera dans le sens trigonométrique. Cliquez à nouveau pour le fixer. 
- ![Accrocher](https://gitlab.com/vincent_jarc/engrenages/-/raw/main/pics/accrocher.png) : permet de connecter deux engrenages entre eux. Cliquez sur un engrenage puis sur l'engrenage cible. La cible ne bouge pas, mais le premier engrenage viendra s'accrocher au second et tournera de manière à imbriquer les dents convenablement. Un intervalle de 2 pixels est toutefois prévu entre les deux engrenages. 
- ![Éteindre](https://gitlab.com/vincent_jarc/engrenages/-/raw/main/pics/pouvoir.png) : permet de fermer l'application. Ne fonctionne pas sous MacOS (voir section [Bugs connus](#bugs-connus)). 

## Licence

Le projet est sous [licence MIT](https://fr.wikipedia.org/wiki/Licence_MIT). Cela signifie que le projet est libre et open source. Vous avez donc le droit de l'utiliser, copier, étudier, modifier, redistribuer… autant que vous le souhaitez. Par correction, pensez toutefois à indiquer son origine. 

<center>

![Licence](https://s1.qwant.com/thumbr/300x0/9/7/0f2eb46b7f8445e02178a98cd5842a81264b6f4f362ff0da94d6629afb35ba/1024px-MIT_logo.svg.png?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F0%2F0c%2FMIT_logo.svg%2F1024px-MIT_logo.svg.png&q=0&b=1&p=0&a=0)

</center>

## Bugs connus

:bug:

- Connecter deux engrenages déjà connectés génère une division par zéro. Déplacez l'un des deux engrenages avant de les reconnecter. 
- Le bouton d'extinction gèle l'application sous MacOS. Ceci est lié à un bug de la bibliothèque `Tkinter` sous MacOS. 
- Bouton `Play` inactif. 

## Roadmap

- proposer un mode Lecture permettant d'animer un engrenage et tous ceux qui lui sont liés ; 
- proposer un outil de capture d'écran (utilisation probable de la bibliothèque `Pillow`) ; 
- permettre d'attribuer une couleur personnalisée aux engrenages ; 
- permettre d'accrocher deux engrenages sur le même moyeu ; 
- permette de relier deux engrenages par une chaîne ou un sangle ; 
- tourner les engrenages de manière plus intuitive ; 