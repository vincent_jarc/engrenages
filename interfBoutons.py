from tkinter import Button, Frame, PhotoImage, Tk, Label, EW, CENTER



class InterfaceBoutons(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.charger_messages()
        self.charger_images()
        self.generer_widgets()
        self.positionner_widgets()
        self.configurer_widgets()
    
    def charger_messages(self):
        self.msg = dict()
        self.msg["ADD"] = "Cliquez sur la feuille\npour ajouter un engrenage.\n\n"
        self.msg["DEL"] = "Cliquez sur un engrenage\npour le supprimer.\n\n"
        self.msg["PPT"] = "Cliquez sur un engrenage\npour voir ses propriétés\nou faites un clic droit.\n"
        self.msg["MOV"] = "Cliquez sur un engrenage\npour le déplacer\npuis cliquez à nouveau\npour le positionner."
        self.msg["SET"] = "Cliquez à l'endroit\noù vous souhaitez\npositionner votre\nengrenage."
        self.msg["TRN"] = "Cliquez sur un engrenage\npour le faire\ntourner.\n"
        self.msg["TRN2"] = "Bougez la souris\npour faire tourner\nl'engrenage et cliquez\npour le fixer."
        self.msg["STK"] = "Cliquez sur un engrenage\npour l'accrocher\nà un autre.\n"
        self.msg["STK2"] = "Cliquez sur un deuxième\nengrenage.\n\n"
        self.msg["OFF"] = "Au revoir."
    
    def charger_images(self):
        self.images = dict()
        self.images["ADD"] = PhotoImage(file = "./pics/plus.png")
        self.images["DEL"] = PhotoImage(file = "./pics/effacer.png")
        self.images["PPT"] = PhotoImage(file = "./pics/reglage.png")
        self.images["MOV"] = PhotoImage(file = "./pics/deplacer.png")
        self.images["TRN"] = PhotoImage(file = "./pics/tourner.png")
        self.images["OFF"] = PhotoImage(file = "./pics/pouvoir.png")
        self.images["STK"] = PhotoImage(file = "./pics/accrocher.png")
        self.images["PLAY"] = PhotoImage(file = "./pics/play-button.png")
    
    def generer_widgets(self):
        self.btn_add    = Button(self)
        self.btn_del    = Button(self)
        self.btn_ppt    = Button(self)
        self.btn_move   = Button(self)
        self.btn_turn   = Button(self)
        self.btn_exit   = Button(self)
        self.btn_stick  = Button(self)
        self.btn_play   = Button(self)
        self.lbl_indic  = Label(self)
        
    def positionner_widgets(self):
        self.btn_add.grid(row=0, column=1)
        self.btn_ppt.grid(row=0, column=2)
        self.btn_move.grid(row=0, column=3)
        self.btn_turn.grid(row=0, column=4)
        self.btn_del.grid(row=0, column=5)
        self.btn_exit.grid(row=0, column=6)
        self.btn_stick.grid(row=1, column=1)
        self.btn_play.grid(row=1, column=6)
        self.lbl_indic.grid(row=3, column=1,
                            columnspan=6, sticky=EW)
        
    def configurer_widgets(self):
        # IMAGES
        self.btn_add["image"]   = self.images["ADD"]
        self.btn_ppt["image"]   = self.images["PPT"]
        self.btn_move["image"]  = self.images["MOV"]
        self.btn_turn["image"]  = self.images["TRN"]
        self.btn_del["image"]   = self.images["DEL"]
        self.btn_exit["image"]  = self.images["OFF"]
        self.btn_stick["image"] = self.images["STK"]
        self.btn_play["image"] = self.images["PLAY"]
        # BACKGROUND
        self.btn_add["bg"]   = "white"
        self.btn_ppt["bg"]   = "white"
        self.btn_move["bg"]  = "white"
        self.btn_turn["bg"]  = "white"
        self.btn_del["bg"]   = "white"
        self.btn_exit["bg"]  = "white"
        self.btn_stick["bg"] = "white"
        self.btn_play["bg"] = "white"
        # BINDINGS
        self.btn_add["command"] = self.on_new_click
        self.btn_ppt["command"] = self.on_ppt_click
        self.btn_move["command"] = self.on_mov_click
        self.btn_turn["command"] = self.on_turn_click
        self.btn_del["command"] = self.on_del_click
        self.btn_exit["command"] = self.master.destroy
        self.btn_stick["command"] = self.on_stick_click
        # LABEL
        self.lbl_indic["text"] = self.msg["ADD"]
        self.lbl_indic["justify"] = CENTER
        self.lbl_indic["font"] = "Tahoma 15"
    
    
    def actualiser(self, msg_id:str=None):
        """msg_id peut valoir ADD, DEL, PPT, MOV, SET, TURN, TURN2, OFF"""
        if msg_id is None:
            self.lbl_indic["text"] = self.msg["ADD"]
        else:
            self.lbl_indic["text"] = self.msg[msg_id]
        
    ##### CALLBACKS #####


    def on_new_click(self):
        self.actualiser("ADD")
        self.master.status = "draw"
        self.master.feuille.illuminer("VERT")
        #self.master.afficher_parametres()
        
    def on_ppt_click(self):
        self.actualiser("PPT")
        self.master.status = "property"
        self.master.feuille.illuminer("GRIS")
        #self.master.afficher_proprietes()
        
    def on_del_click(self):
        self.actualiser("DEL")
        self.master.status = "delete"
        self.master.feuille.illuminer("ROUGE")
        #self.master.afficher_proprietes()
        
    def on_mov_click(self):
        self.actualiser("MOV")
        self.master.status = "move-1"
        self.master.feuille.illuminer("JAUNE")
        #self.master.afficher_proprietes()
        
    def on_turn_click(self):
        self.actualiser("TRN")
        self.master.status = "turn-1"
        self.master.feuille.illuminer("JAUNE")
        #self.master.afficher_proprietes()
    
    def on_stick_click(self):
        self.actualiser("STK")
        self.master.status = "stick-1"
        self.master.feuille.illuminer("BLEU")
        
        
        
        
if __name__ == "__main__":
    f = Tk()
    i = InterfaceBoutons(f)
    i.grid()
    f.mainloop()