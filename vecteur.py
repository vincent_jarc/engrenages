from math import pi, cos, sin, sqrt, acos


class Vecteur:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __str__(self):
        return f"Vecteur({self.x},{self.y})"
    
    def __repr__(self):
        return f"Vecteur({self.x},{self.y})"
    
    def __neg__(self):
        return Vecteur(-self.x, -self.y)
    
    def __add__(self, other):
        return Vecteur(self.x + other.x, self.y + other.y)
    
    def __sub__(self, other):
        return self + (-other)
    
    def norme(self) -> float:
        return sqrt(self.x**2 + self.y**2)
    
    def normaliser(self):
        d = self.norme()
        self.x /= d
        self.y /= d

    def rotation(self, angle, centre):
        """
        Tourne dans sens horaire, angle en radian
        [enlever le - devant angle si problème]
        """
        v = self - centre
        x = v.x*cos(-angle) - v.y*sin(-angle)
        y = v.x*sin(-angle) + v.y*cos(-angle)
        w = Vecteur(x,y)
        return centre + w
    
    def set(self, x, y):
        """Réécris les coordonnées du vecteur"""
        self.x = x
        self.y = y


def entier(v):
    return Vecteur(round(v.x), round(v.y))

def generer_carre(A:Vecteur, B:Vecteur)->list:
    C = A.rotation(pi/2, B)
    D = B.rotation(-pi/2, A)
    return [A,D,C,B]

def produit_scalaire(u:Vecteur, v:Vecteur)->float:
    return u.x*v.x + u.y*v.y

def angle(u:Vecteur, v:Vecteur)->float:
    return acos(produit_scalaire(u,v)/(u.norme()*v.norme()))

if __name__ == "__main__":
    O = Vecteur(0,8)
    M = Vecteur(6,0)
    print(generer_carre(O,M))
    print(angle(O,M), angle(O,M)*180/pi, "°")
    print(angle(M,O), angle(M,O)*180/pi, "°")
    print(acos(-0.5)*180/pi)
    