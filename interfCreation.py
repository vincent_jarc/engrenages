from tkinter import Tk, Spinbox, Scale, EW, Frame
from tkinter import HORIZONTAL, LEFT, CENTER, Event, Label
from math import sqrt, pi

class InterfaceCreation(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.generer_widgets()
        self.positionner_widgets()
        self.configurer_widgets()
    
    def generer_widgets(self):
        self.lbl_dents  = Label(self)
        self.spin_dents = Spinbox(self)
        self.lbl_orient  = Label(self)
        self.scl_orient = Scale(self)
        
    def positionner_widgets(self):
        self.lbl_dents.grid(row=0, column=1, sticky=EW)
        self.spin_dents.grid(row=1, column=1, sticky=EW)
        self.lbl_orient.grid(row=2, column=1, sticky=EW)
        self.scl_orient.grid(row=3, column=1, sticky=EW)
    
    def configurer_widgets(self):
        # Labels
        self.lbl_dents["text"] = "Nombre de dents"
        self.lbl_dents["font"] = ("TkDefaultFont", 12)
        self.lbl_orient["text"] = "Orientation"
        self.lbl_orient["font"] = ("TkDefaultFont", 12)
        # Spinbox Nb dents
        self.spin_dents["to"]        = 25
        self.spin_dents["from_"]     = 3
        self.spin_dents["increment"] = 1
        self.spin_dents["command"] = lambda : self.master.barre_actions.on_new_click()
        # Scale Orientation
        self.scl_orient["orient"] = HORIZONTAL
        self.scl_orient["to"] = 360
        self.scl_orient["from_"] = 0
        self.scl_orient["resolution"] = 1
        self.scl_orient["tickinterval"] = 90
        self.scl_orient["length"] = 200
        self.scl_orient["command"]=lambda evt : self.master.barre_actions.on_new_click()
    
    def afficher(self):
        self.grid()
    
    def cacher(self):
        self.grid_remove()
    
    def get_dents(self):
        """Renvoie le nombre de dents sélectionné"""
        return int(self.spin_dents.get())
    
    def get_angle(self):
        """Renvoie l'angle sélectionné"""
        return float(self.scl_orient.get())*pi/180
    
    def get(self):
        """Renvoie le nombre de dents et l'angle sélectionnés"""
        return self.get_dents(), self.get_angle()

if __name__ == "__main__":
    f = Tk()
    i = InterfaceCreation(f)
    i.grid()
    f.mainloop()