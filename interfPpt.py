from tkinter import Label, Tk, CENTER
from engrenage import Engrenage


class InterfaceProprietes(Label):
    def __init__(self, master):
        Label.__init__(self, master)
        self.default = "Aucun engrenage sélectionné"
        self["text"] = self.default
        self["justify"] = CENTER
        self["font"] = "Tahoma 15"
        
    def actualiser(self, eng:Engrenage=None):
        if eng is not None:
            self["text"] = str(eng)
        else:
            self["text"] = self.default


    def afficher(self):
        self.grid()


    def cacher(self):
        self.grid_remove()


if __name__ == "__main__":
    f = Tk()
    i = InterfaceProprietes(f)
    i.grid()
    f.mainloop()