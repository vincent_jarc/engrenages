from tkinter import Tk, EW, Event
#from tkinter.simpledialog import askinteger
#from PIL import ImageGrab
from math import sqrt
from engrenage import *
from interfDessin import InterfaceDessin
from interfBoutons import InterfaceBoutons
from interfPpt import InterfaceProprietes
from interfCreation import InterfaceCreation


## TODO : Revoir la déconnexion des engrenages connectés


## CAPTURE ECRAN
"""
from PIL import ImageGrab
import tkinter as tk
 
root = tk.Tk()
root.configure(bg='blue')
x, y, w, h = 100, 100, 200, 200
root.geometry('%dx%d+%d+%d' % (w, h, x, y))
tk.Canvas(root, width=100, height=100, bg='grey').grid(row=0, column=0)
tk.Canvas(root, width=100, height=100, bg='green').grid(row=1, column=1)
 
root.update()
x = root.winfo_rootx()
y = root.winfo_rooty()
 
im = ImageGrab.grab((x, y, x+w, y+h))
im.save('test.png')
im.show()
"""



class Application(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.selection = None
        self.status = "draw"
        self.engrenages = Systeme()
        self.generate()
        self.configure()
        #self.afficher_parametres()
        self.mainloop()
    
    def generate(self):
        # Création des widgets
        self.feuille = InterfaceDessin(self)
        self.barre_actions = InterfaceBoutons(self)
        self.menu_ppt = InterfaceProprietes(self)
        self.menu_creation = InterfaceCreation(self)
        # Placement des widgets
        self.feuille.grid(row=0, column=0, rowspan=10)
        self.barre_actions.grid(row=0, column=1)
        self.menu_creation.grid(row=1, column=1, sticky=EW)
        self.menu_ppt.grid(row=2, column=1, sticky=EW)
    
    def configure(self):
        self.title("Engrenage")
        # Canva
        self.feuille.illuminer("VERT")
        self.feuille.bind("<Button-1>", self.on_click)
        self.feuille.bind("<Button-2>", self.on_click)
        self.feuille.bind("<Motion>", self.on_motion)
        


    ##### MÉTHODES DE BASE #####

    def ajouter_engrenage(self, evt:Event):
        centre = evt.x, evt.y
        nb_dents, orientation = self.menu_creation.get()
        e = self.engrenages.ajouter(nb_dents, centre, orientation)
        self.feuille.dessiner_engrenage(e)


    def supprimer_engrenage(self, evt:Event):
        if self.trouver_engrenage(evt):
            self.feuille.effacer_engrenage(self.selection)
            eng = self.engrenages.get(self.selection["num_engrenage"])
            self.engrenages.supprimer(eng)


    def trouver_engrenage(self, evt:Event) -> bool:
        """
        sélectionne un engrenage et les dessins de dents
        et de moyeu correspondant et renvoie un booléen
        indiquant si une sélection a été possible.
        WARNING : le clic doit se faire à moins de 10px
        d'un engrenage. 
        """
        if self.engrenages.est_vide():
            return False
        x = self.feuille.find_closest(evt.x, evt.y)[0]
        num_engrenage = self.feuille.dessins[x]["num_engrenage"]
        eng = self.engrenages.get(num_engrenage) # !!!
        D = sqrt((evt.x - eng.centre.x)**2 + (evt.y - eng.centre.y)**2)
        d = eng.rayon + eng.cote
        distance = D - d
        if distance >= 10:
            self.selection = None
            return False
        elif self.feuille.dessins[x]["type"] == "moyeu":
            y = self.feuille.dessins[x]["num_dents"]
            self.selection = {"num_engrenage":num_engrenage,
                              "num_dents":y,
                              "num_moyeu":x}
            return True
        else:
            y = self.feuille.dessins[x]["num_moyeu"]
            self.selection = {"num_engrenage":num_engrenage,
                              "num_dents":x,
                              "num_moyeu":y}
            return True
        
    
    def actualiser_proprietes(self):
        if self.selection is None:
            e = None
        else:
            e = self.engrenages.get(self.selection["num_engrenage"])
        self.menu_ppt.actualiser(e)
        #self.menu_ppt.afficher()
        #self.menu_creation.cacher()
    
    '''def afficher_parametres(self):
        self.menu_ppt.cacher()
        self.menu_creation.afficher()
    
    def effacer_proprietes(self):
        self.menu_ppt.cacher()'''

        
    ##### CALLBACKS #####
        
    def on_click(self, evt:Event):
        if self.status == "draw" and evt.num == 1:
            self.ajouter_engrenage(evt)
        elif self.status == "delete" and evt.num == 1:
            self.supprimer_engrenage(evt)
        elif self.status == "property" or evt.num == 2:
            self.trouver_engrenage(evt)
            self.actualiser_proprietes()
        elif self.status == "move-1" and evt.num == 1:
            self.translater_engrenage(evt)
        elif self.status == "move-2" and evt.num == 1:
            self.arreter_translation(evt)
        elif self.status == "turn-1" and evt.num == 1:
            self.tourner_engrenage(evt)
        elif self.status == "turn-2" and evt.num == 1:
            self.arreter_rotation(evt)
        elif self.status in ["stick-1", "stick-2"] and evt.num == 1:
            self.connecter(evt)

    def on_motion(self, evt:Event):
        if self.status == "move-2":
            self.suivre_souris(evt)
        elif self.status == "turn-2":
            self.suivre_angle_souris(evt)


    #### TRANSLATER UN ENGRENAGE #####


    def translater_engrenage(self, evt):
        if self.trouver_engrenage(evt):
            self.actualiser_proprietes()
            self.barre_actions.actualiser("SET")
            self.status = "move-2"

    def suivre_souris(self, evt):
        assert self.status == "move-2", "Le status n'est pas 'move-2' mais"+self.status
        num = self.selection["num_engrenage"]
        x = self.selection["num_dents"]
        y = self.selection["num_moyeu"]
        eng = self.engrenages.get(num)
        self.engrenages.positionner(eng, evt.x, evt.y)
        #eng.positionner(evt.x, evt.y)
        self.feuille.actualiser(eng, x, y)
        self.actualiser_proprietes()

    def arreter_translation(self, evt):
        assert self.status == "move-2", "Le status n'est pas 'move-2' mais"+self.status
        self.barre_actions.actualiser("MOV")
        self.status = "move-1"
        
        num = self.selection["num_engrenage"]
        x = self.selection["num_dents"]
        y = self.selection["num_moyeu"]
        
        eng = self.engrenages.get(num)
        #eng.positionner(evt.x, evt.y)
        self.engrenages.positionner(eng, evt.x, evt.y)
        self.feuille.actualiser(eng,x,y)


    #### FAIRE TOURNER UN ENGRENAGE #####


    def tourner_engrenage(self, evt):
        if self.trouver_engrenage(evt):
            self.actualiser_proprietes()
            self.barre_actions.actualiser("TRN2")
            self.status = "turn-2"

    def suivre_angle_souris(self, evt):
        assert self.status == "turn-2", "Le status n'est pas 'turn-2' mais"+self.status
        num = self.selection["num_engrenage"]
        #x = self.selection["num_dents"]
        #y = self.selection["num_moyeu"]
        eng = self.engrenages.get(num)
        self.engrenages.tourner(eng, 1*pi/180)
        #self.feuille.actualiser(eng,x,y)
        self.feuille.actualiser_tout(self.engrenages)
        self.actualiser_proprietes()

    def arreter_rotation(self, evt):
        self.barre_actions.actualiser("TRN")
        self.status = "turn-1"

    def connecter(self, evt):
        #Sauvegarde de la précédente sélection
        if self.selection is not None:
            old_num = self.selection["num_engrenage"]
            old_x = self.selection["num_dents"]
            old_y = self.selection["num_moyeu"]
            old_eng = self.engrenages.get(old_num)
        #On regarde si le clic est valide
        if self.trouver_engrenage(evt):
            if self.status == "stick-1":
                #Si c'est le 1er clic, on passe en mode 2
                self.status = "stick-2"
                self.barre_actions.actualiser("STK2")
            elif self.status == "stick-2":
                #Si c'est le 2ème clic, on accroche et on repasse en mode 1
                new_num = self.selection["num_engrenage"]
                new_eng = self.engrenages.get(new_num)
                if old_num != new_num:
                    #old_eng.accrocher(new_eng)
                    self.engrenages.connecter(old_eng, new_eng)
                    self.feuille.actualiser(old_eng,old_x,old_y)
                    self.actualiser_proprietes()
                    self.barre_actions.actualiser("STK")
                    self.status = "stick-1"
        else: #si le clic n'est pas valide on revient au status initial
            self.status = "stick-1"
            self.barre_actions.actualiser("STK")



if __name__ == "__main__":
    app = Application()