from math import pi, cos, sin, tan, sqrt
from random import choice
from vecteur import *

class Engrenage:
    def __init__(self,
                 nb_dents:int,
                 centre:tuple,
                 orientation:float=0.0):
        """
        WARNING :
            - orientation : angle en radian orienté dans le
            sens trigonométrique (à l'écran)
            - centre : couple d'entiers
            - 
        """
        self.cote         = 25
        self.id           = id(self)
        self.nb_dents     = nb_dents
        self.centre       = Vecteur(*centre)
        self.orientation  = orientation
        self.nb_cotes     = 2*self.nb_dents
        self.angle_centre = pi / self.nb_dents
        self.rayon        = self.cote / 2 / sin(self.angle_centre/2)
        self.moyeu        = self.rayon / 3
        self.hauteur      = self.cote / 2 / tan(self.angle_centre/2)
        self.generer_couleurs()
        self.generer_points()
    
    def __str__(self):
        txt = f"Engrenage n°{self.id}\n"
        txt += f"Dents  :\t{self.nb_dents}°\t\n"
        txt += f"Angle  :\t{round(self.orientation*180/pi)}°\t\n"
        txt += f"Centre :\t({self.centre.x},{self.centre.y})\t\n"
        return txt
    
    def __repr__(self):
        return f"n={self.nb_cotes}, a= {self.angle_centre*180/pi}, h={self.hauteur}, r={self.rayon}"
    
    def generer_polygone(self):
        point = Vecteur(self.centre.x + self.cote/2,
                        self.centre.y - self.hauteur)
        polygone = [point.rotation(self.orientation,self.centre)]
        for _ in range(self.nb_cotes-1):
            polygone.append(
                polygone[-1].rotation(self.angle_centre, self.centre))
        return polygone
    
    def generer_points(self):
        """
        Crée la liste des points de l'enveloppe connexe de
        l'engrenage. Pour une orientation de 0rad, le premier
        point est dans le creux en haut à droite, le second sur
        la dent du haut, à droite. Les autres points sont
        construits dans le sens trigo. 
        """
        self.points = []
        polygone = self.generer_polygone()
        for i in range(0, len(polygone),2):
            self.points.extend(generer_carre(polygone[i], polygone[i+1]))
            
    def points_dents(self):
        return [(v.x, v.y) for v in self.points]
    
    def points_moyeu(self):
        delta = sqrt(2)/2*self.moyeu
        return [self.centre.x-delta, self.centre.y-delta,
                self.centre.x+delta, self.centre.y+delta]
    
    def couleur_principale(self, color:str=None):
        """
        Renvoie la couleur principale si aucune couleur
        n'est renseignée. Sinon, modifie la couleur principale
        """
        if color is None:
            return self.color1
        else:
            self.color1 = color
    
    def couleur_secondaire(self, color:str=None):
        """
        Renvoie la couleur secondaire si aucune couleur
        n'est renseignée. Sinon, modifie la couleur secondaire
        """
        if color is None:
            return self.color2
        else:
            self.color2 = color
    
    def generer_couleurs(self):
        """
        Génère deux couleurs aléatoires
        WARNING : color2 est 20% plus clair que color1
        """
        color1 = "#"
        for _ in range(6):
            color1 += choice("0123456789ABCDEF")
        r, v, b = int(color1[1:3], 16), int(color1[3:5],16), int(color1[5:7],16)
        r = str(hex(min(int(1.2*r), 255)))[2:]
        v = str(hex(min(int(1.2*v), 255)))[2:]
        b = str(hex(min(int(1.2*b), 255)))[2:]
        if len(r) == 1: r="0"+r
        if len(v) == 1: v="0"+v
        if len(b) == 1: b="0"+b
        color2 = "#"+r+v+b
        self.color1 = color1
        self.color2 = color2
    
    def couleurs(self, colors:tuple=None):
        """
        Renvoie la couleur principale et la couleur secondaire
        si aucune couleur n'est renseignée. Sinon, modifie les
        deux couleurs de l'engrenages
        """
        if colors is None:
            return self.color1, self.color2
        elif len(colors) == 2:
            self.color1, self.color2 = colors
            
    def positionner(self, x, y):
        """Place le centre de l'engrenage aux
        coordonnées (x,y)"""
        self.centre.set(x, y)
        self.generer_points()
    
    def deplacer(self, vec:Vecteur):
        """Déplace l'engrenage suivant le vecteur vec"""
        x = self.centre.x + vec.x
        y = self.centre.y + vec.y
        self.positionner(x,y)
        
    
    def tourner(self, angle):
        """Tourne l'engrenage de la valeur de l'angle.
        WARNING:angle doit être en radians"""
        self.orientation += angle
        while self.orientation > 2*pi:
            self.orientation -= 2*pi
        self.generer_points()
    
    def orienter(self, angle):
        """Fixe l'orientation de l'engrenage à la
        valeur de l'angle. 0 correspond à la verticale. 
        WARNING:angle doit être en radians"""
        self.orientation = angle
        self.generer_points()
    
    def accrocher(self, other):
        """Accroche l'engrenage self à l'engrenage other. self
        est déplacé et réorienté. other n'est pas modifié."""
        vec = other.centre - self.centre
        # On rapproche les engrenages
        d = vec.norme() - (2+self.rayon+other.rayon+self.cote)
        vec.normaliser()
        vec.x *= d
        vec.y *= d
        vec = entier(vec)
        self.deplacer(vec)
        # On oriente l'engrenage self en direction de other
        a1 = angle(Vecteur(0,-1), vec)
        if vec.x >= 0 :
            a1 = -a1
        self.orienter(a1)
        # On On tourne self pour que les engrenages s'emboîtent
        # si a2=0rad, on est pile sur une dent
        a2 = (pi+a1-other.orientation)%(2*other.angle_centre) 
        a3 = a2
        self.tourner(self.angle_centre + a2*other.nb_dents/self.nb_dents)



class Systeme:
    def __init__(self):
        self.datas = dict()
    
    def est_vide(self)->bool:
        return len(self.datas) == 0
    
    def get(self, eng_id:int):
        assert eng_id in self.datas, f"L'engrenage {eng_id} n'est pas présent"
        return self.datas[eng_id]["eng"]        

    def ajouter(self, nb_dents:int,
                centre:tuple, orientation:float=0.0):
        eng = Engrenage(nb_dents, centre, orientation)
        self.datas[eng.id] = {"eng":eng, "succ":set()}
        return eng

    def supprimer(self, eng:Engrenage):
        assert eng.id in self.datas, f"Engrenage {eng.id} inexistant"
        self.deconnecter(eng)
        del(self.datas[eng.id])

    def pere_de(self, eng:Engrenage)->Engrenage:
        """Renvoie l'identifiant de l'engrenage père de eng"""
        for id_engre in self.datas:
            if eng.id in self.datas[id_engre]["succ"]:
                return self.datas[id_engre]["eng"]

    def est_pere(self, fils:Engrenage, pere:Engrenage)->bool:
        return fils.id in self.datas[pere.id]["succ"]

    def ancetre_de(self, eng:Engrenage)->Engrenage:
        if self.pere_de(eng) is None:
            return eng
        else:
            return self.ancetre_de(self.pere_de(eng))

    def connecter(self, eng1:Engrenage, eng2:Engrenage):
        """Connecte eng1 (fils) à eng2 (père)"""
        assert eng1.id in self.datas, f"Engrenage {eng1.id} inexistant"
        assert eng2.id in self.datas, f"Engrenage {eng2.id} inexistant"
        eng1.accrocher(eng2)
        self.datas[eng2.id]["succ"].add(eng1.id)

    def deconnecter(self, eng:Engrenage):
        """Déconnecte un engrenage de son père et de ses fils"""
        assert eng.id in self.datas, f"Engrenage {eng.id} inexistant"
        self.datas[eng.id]["succ"] = set()
        pere = self.pere_de(eng)
        if pere is not None:
            self.datas[pere.id]["succ"].remove(eng.id)

    def tourner(self, eng:Engrenage, angle):
        def tourner_rec(self, eng:Engrenage, angle):
            eng.tourner(angle)
            for id_fils in self.datas[eng.id]["succ"]:
                fils = self.get(id_fils)
                rapport = eng.nb_dents / fils.nb_dents
                tourner_rec(self, fils, - rapport*angle)
        old = self.ancetre_de(eng)
        tourner_rec(self, old, angle)

    def deplacer(self, eng:Engrenage, vec:Vecteur):
        self.deconnecter(eng)
        eng.deplacer(vec)

    def positionner(self, eng:Engrenage, x, y):
        self.deconnecter(eng)
        eng.positionner(x,y)
        


if __name__ == "__main__":
    s = Systeme()
    e1 = s.ajouter(3, (0, 0), 0.0)
    e2 = s.ajouter(5, (10, 45), 0.0)
    e3 = s.ajouter(4, (20, 32), 0.0)
    s.connecter(e2, e1)
    s.connecter(e3, e2)
    print(s.datas)
    print("------\n", f"Rotation de {e1.id}:\n")
    s.tourner(e2, 1)
    print("------\n", f"Rotation de {e1.id}:\n")
    s.deconnecter(e2)
    s.tourner(e2, -1)
    
    