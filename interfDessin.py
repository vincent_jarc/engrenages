from tkinter import Tk, Canvas
from engrenage import *





class InterfaceDessin(Canvas):
    def __init__(self, master):
        Canvas.__init__(self, master=master)
        self.dessins = dict()
        self["bg"]="white"
        self["width"]=1000
        self["height"]=900
        self["highlightthickness"] = 10
        self["highlightbackground"] = "#43A343"
        
    ##### MÉTHODES DE BASE #####

    def dessiner_engrenage(self, eng:Engrenage):
        d = self.create_polygon(eng.points_dents(),
                                fill=eng.couleur_principale(),
                                activefill=eng.couleur_secondaire(),
                                outline="black",
                                width=4)
        m = self.create_oval(eng.points_moyeu(),
                             fill="white",
                             outline="black",
                             width=4)
        self.dessins[d] = {"type":"dents",
                           "num_engrenage":eng.id,
                           "num_dents":d,
                           "num_moyeu":m}
        self.dessins[m] = {"type":"moyeu",
                           "num_engrenage":eng.id,
                           "num_dents":d,
                           "num_moyeu":m}

    def actualiser(self, eng:Engrenage, num_dents:int, num_moyeu:int):
        def flatten(L):
            """Transforme une liste de tuples en liste"""
            res = []
            for couple in L:
                res.extend(couple)
            return res
        self.coords(num_dents, flatten(eng.points_dents()))
        self.coords(num_moyeu, eng.points_moyeu())
        
    def actualiser_tout(self, sys:Systeme):
        for eng_id in sys.datas:
            eng = sys.get(eng_id)
            d, m = self.get_by_id(eng_id)
            self.actualiser(eng, d, m)

    def get_by_id(self, ID):
        for k in self.dessins:
            if self.dessins[k]["num_engrenage"] == ID:
                d = self.dessins[k]["num_dents"]
                m = self.dessins[k]["num_moyeu"]
                return d, m
        
    
    def effacer_engrenage(self, selection):
        num = selection["num_engrenage"]
        x   = selection["num_dents"]
        y   = selection["num_moyeu"]
        self.dessins[x] = None
        self.dessins[y] = None
        self.delete(x,y)
    
    def illuminer(self, color:str="VERT"):
        assert color in ["VERT", "ROUGE", "JAUNE", "GRIS", "BLEU"]
        if color == "VERT":
            self["highlightbackground"] = "#43A343"
        elif color == "ROUGE":
            self["highlightbackground"] = "#FF0000"
        elif color == "JAUNE":
            self["highlightbackground"] = "#FFC409"
        elif color == "GRIS":
            self["highlightbackground"] = "#343637"
        elif color == "BLEU":
            self["highlightbackground"] = "#1E81F0"

            
        


if __name__ == "__main__":
    from math import pi
    e = Engrenage(6, (100, 500))
    f = Tk()
    i = InterfaceDessin(f)
    i.pack()
    i.dessiner_engrenage(e)
    i.illuminer("GRIS")
    #e.positionner(800, 500)
    #e.tourner(10)
    #i.actualiser(e,1,2)
    f.mainloop()